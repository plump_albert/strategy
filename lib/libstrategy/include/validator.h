// vim:ft=cpp:ts=4:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
#ifndef LIBSTRATEGY_SRC_VALIDATOR_H
#define LIBSTRATEGY_SRC_VALIDATOR_H
#include <string>

class Validator {
protected:
  Validator();
  ~Validator();

public:
  virtual bool validate(const std::string &str) = 0;
};

#endif // LIBSTRATEGY_SRC_VALIDATOR_H
