#include "strategy.h"
#include "string.h"
#include "validators/lower_case.h"
#include "validators/number.h"
#include "validators/upper_case.h"
#include <iostream>

char usage() {
  char answer;
  std::cout << "Input type [ (n)umber, (u)pper, (l)ower, e(x)it ]: ";
  std::cin >> answer;
  return answer;
}

int main() {
  StrategyValidator validator;
  char answer;
  char input[256];
  answer = usage();
  while (answer != 'x') {
    switch (answer) {
    case 'n':
      validator.setValidator(new NumberValidator('.'));
      break;
    case 'u':
      validator.setValidator(new UpperCaseValidator());
      break;
    case 'l':
      validator.setValidator(new LowerCaseValidator());
      break;
    default:
      return 1;
    }
    std::cout << "To quit from validation type 'quit'" << std::endl << "# ";
    std::cin >> input;
    while (strcmp(input, "quit")) {
      if (validator.validate(input)) {
        std::cout << "*** good ***" << std::endl;
      } else {
        std::cout << "*** bad  ***" << std::endl;
      }
      std::cout << "# ";
      std::cin >> input;
    }
    answer = usage();
  }
  return 0;
}
