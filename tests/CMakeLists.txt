# Add Google test library
include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/609281088cfefc76f9d0ce82e1ff6c30cc3591e5.zip
)
# For windows chads: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)
enable_testing()

# Test
add_executable(
  test
  test.cpp
)
target_link_libraries(
  test
  gtest_main
  libstrategy
)

include(GoogleTest)
gtest_discover_tests(test)
