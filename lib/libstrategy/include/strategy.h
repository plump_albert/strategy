#ifndef LIBSTRATEGY_INCLUDE_STRATEGY_H
#define LIBSTRATEGY_INCLUDE_STRATEGY_H
#include "validator.h"

class StrategyValidator {
  Validator *m_validator = nullptr;

public:
  StrategyValidator();
  StrategyValidator(Validator *validator);
  bool validate(const char *input) const;
  void setValidator(Validator *validator);
};

#endif // LIBSTRATEGY_INCLUDE_STRATEGY_H
