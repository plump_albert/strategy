#include "strategy.h"
#include "validators/number.h"
#include "validators/lower_case.h"
#include "validators/upper_case.h"
#include <gtest/gtest.h>

/**
 * @brief Test input to be a number
 */
TEST(ValidateNumber, TestInteger) {
  const char *input = "146";
  const auto validator = new StrategyValidator(new NumberValidator());
  EXPECT_EQ(validator->validate(input), true);
}
TEST(ValidateNumber, TestFloatNumber) {
  const char *input = "14.6";
  const auto validator = new StrategyValidator(new NumberValidator('.'));
  EXPECT_EQ(validator->validate(input), true);
}
TEST(ValidateNumber, TestFloatNumberWithMultiplePoints) {
  const char *input = "14.6.5";
  const auto validator = new StrategyValidator(new NumberValidator('.'));
  EXPECT_EQ(validator->validate(input), false);
}
TEST(ValidateNumber, TestWrongInput) {
  const char *input = "abc";
  const auto validator = new StrategyValidator(new NumberValidator(','));
  EXPECT_EQ(validator->validate(input), false);
}

/**
 * @brief Test suite for testing lowercase validation
 */
TEST(ValidateLowerCase, TestLowerCase) {
  const char *input = "hello world";
  const auto validator = new StrategyValidator(new LowerCaseValidator());
  EXPECT_EQ(validator->validate(input), true);
}
TEST(ValidateLowerCase, TestMixedCase) {
  const char *input = "Hello World";
  const auto validator = new StrategyValidator(new LowerCaseValidator());
  EXPECT_EQ(validator->validate(input), false);
}
TEST(ValidateLowerCase, TestUpperCase) {
  const char *input = "HELLO WORLD";
  const auto validator = new StrategyValidator(new LowerCaseValidator());
  EXPECT_EQ(validator->validate(input), false);
}

/**
 * @brief Test suite for testing uppercase validation
 */
TEST(ValidateUpperCase, TestLowerCase) {
  const char *input = "hello world";
  const auto validator = new StrategyValidator(new UpperCaseValidator());
  EXPECT_EQ(validator->validate(input), false);
}
TEST(ValidateUpperCase, TestMixedCase) {
  const char *input = "Hello World";
  const auto validator = new StrategyValidator(new UpperCaseValidator());
  EXPECT_EQ(validator->validate(input), false);
}
TEST(ValidateUpperCase, TestUpperCase) {
  const char *input = "HELLO WORLD";
  const auto validator = new StrategyValidator(new UpperCaseValidator());
  EXPECT_EQ(validator->validate(input), true);
}
