#ifndef LIBSTRATEGY_INCLUDE_VALIDATORS_LOWERCASE_H
#define LIBSTRATEGY_INCLUDE_VALIDATORS_LOWERCASE_H
#include "validator.h"

class LowerCaseValidator: public Validator {
  public:
    bool validate(const std::string &str);
};

#endif // LIBSTRATEGY_INCLUDE_VALIDATORS_LOWERCASE_H
