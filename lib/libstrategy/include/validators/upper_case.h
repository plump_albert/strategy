#ifndef LIBSTRATEGY_INCLUDE_VALIDATORS_UPPERCASE_H
#define LIBSTRATEGY_INCLUDE_VALIDATORS_UPPERCASE_H
#include "validator.h"

class UpperCaseValidator: public Validator {
  public:
    bool validate(const std::string &str);
};

#endif // LIBSTRATEGY_INCLUDE_VALIDATORS_UPPERCASE_H
