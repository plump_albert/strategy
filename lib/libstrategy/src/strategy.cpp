#include "../include/strategy.h"

StrategyValidator::StrategyValidator() { this->m_validator = nullptr; }
StrategyValidator::StrategyValidator(Validator *validator) {
  this->m_validator = validator;
}

bool StrategyValidator::validate(const char *input) const {
  if (!this->m_validator) {
    throw "No validator is provided!";
  }
  return this->m_validator->validate(input);
}

void StrategyValidator::setValidator(Validator *validator) {
  this->m_validator = validator;
}
