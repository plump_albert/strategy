#include "validators/number.h"

#define ZERO_ASCII 0x30
#define NINE_ASCII 0x39

NumberValidator::NumberValidator(char decimal_separator) {
  this->decimal_separator = (unsigned char)decimal_separator;
}

bool NumberValidator::validate(const std::string &str) {
  int decimal_point_count = 0;
  for (size_t i = 0; i < str.length(); ++i) {
    const unsigned char symbol = str[i];
    if (symbol < ZERO_ASCII || symbol > NINE_ASCII) {
      if (symbol == this->decimal_separator) {
        ++decimal_point_count;
        if (decimal_point_count > 1) {
          return false;
        }
      } else if (symbol != this->decimal_separator) {
        return false;
      }
    }
  }
  return true;
}
