#ifndef LIBSTRATEGY_SRC_VALIDATORS_NUMBER_H
#define LIBSTRATEGY_SRC_VALIDATORS_NUMBER_H

#include "../validator.h"

class NumberValidator : public Validator {
  unsigned char decimal_separator;
public:
  NumberValidator(char decimal_separator = '.');
  bool validate(const std::string &str);
};

#endif // LIBSTRATEGY_SRC_VALIDATORS_NUMBER_H
