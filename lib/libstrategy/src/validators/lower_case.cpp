#include "validators/lower_case.h"

#define LOWER_A 0x61
#define LOWER_Z 0x7A
#define SPACE 0x20

bool LowerCaseValidator::validate(const std::string &str) {
  for (size_t i = 0; i < str.length(); ++i) {
    const unsigned char symbol = str[i];
    if ((symbol < LOWER_A || symbol > LOWER_Z) && symbol != SPACE) {
      return false;
    }
  }
  return true;
}
