#include "validators/upper_case.h"

#define UPPER_A 0x41
#define UPPER_Z 0x5A
#define SPACE 0x20

bool UpperCaseValidator::validate(const std::string &str) {
  for (size_t i = 0; i < str.length(); ++i) {
    const unsigned char symbol = str[i];
    if ((symbol < UPPER_A || symbol > UPPER_Z) && symbol != SPACE) {
      return false;
    }
  }
  return true;
}
